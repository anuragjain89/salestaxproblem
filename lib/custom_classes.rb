module CustomClasses

  module CustomValidations

    def validate_product_name(name)
      raise CustomExceptions::BlankValueError.new('name') if name.to_s.length == 0
    end

    def validate_imported(imported)
      raise CustomExceptions::BlankValueError.new('imported') if imported.to_s.length == 0
      raise CustomExceptions::YesNoValueError.new('imported') unless imported.chomp.downcase =~ /^(yes|no)$/
    end

    def validate_sales_tax_exempted(sales_tax_exempted)
      raise CustomExceptions::BlankValueError.new('sales tax exempted') if sales_tax_exempted.to_s.length == 0
      raise CustomExceptions::YesNoValueError.new('sales tax exempted') unless sales_tax_exempted.chomp.downcase  =~ /^(yes|no)$/
    end

    def validate_price(price)
      raise CustomExceptions::BlankValueError.new('price') if price.to_s.length == 0
      raise CustomExceptions::NegativeValueError.new('price') if price.to_f < 0
      raise CustomExceptions::NonNumericValueError.new('price') if price.to_s =~ /\'^[\d\.]+$\'/
    end

    def validate_user_option(option)
      raise CustomExceptions::BlankValueError.new('option') if option.to_s.length == 0
      raise CustomExceptions::YesNoValueError.new('option') unless option.chomp.downcase  =~ /^(y|n)$/

    end

  end

  module CustomExceptions

    class BlankValueError < ArgumentError
      attr_reader :error_message
      def initialize(public_name = 'value')
        @error_message = "The #{ public_name } can not be blank"
      end
    end

    class NegativeValueError < ArgumentError
      attr_reader :error_message
      def initialize(public_name = 'value')
        @error_message = "The #{ public_name } can not be negative"
      end
    end

    class YesNoValueError < ArgumentError
      attr_reader :error_message
      def initialize(public_name = 'value')
        @error_message = "The #{ public_name } should be entered as yes / no"
      end
    end

    class NonNumericValueError < ArgumentError
      attr_reader :error_message
      def initialize(public_name = 'value')
        @error_message = "The #{ public_name } should be a numeric value"
      end
    end

    class YNValueError < ArgumentError
      attr_reader :error_message
      def initialize(public_name = 'value')
        @error_message = "The #{ public_name } should be entered as y / n"
      end
    end

  end

end