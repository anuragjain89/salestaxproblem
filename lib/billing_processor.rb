require_relative '../lib/custom_classes.rb'
require_relative '../lib/product.rb'

class BillingProcessor

  attr_accessor :add_more_items
  def initialize
    @add_more_items = 'y'
  end

  include CustomClasses::CustomExceptions
  include CustomClasses::CustomValidations

  def begin_transaction
    product_bucket = load_product_bucket
    clear_screen
    display_transaction_summary(product_bucket)
  end

  private

  def get_product_name
    begin
      print 'Name of the product : '
      product_name = gets.chomp
      validate_product_name(product_name)
    rescue BlankValueError => e
      process_rescue(e)
      retry
    end
    product_name
  end

  def get_product_imported
    begin
      print 'Imported? : '
      imported = gets.chomp
      validate_imported(imported)
    rescue BlankValueError, YesNoValueError => e
      process_rescue(e)
      retry
    end
    imported
  end

  def get_sales_tax_exempted
    begin
      print 'Exempted from sales tax? : '
      sales_tax_exempted = gets.chomp
      validate_sales_tax_exempted(sales_tax_exempted)
    rescue BlankValueError, YesNoValueError => e
      process_rescue(e)
      retry
    end
    sales_tax_exempted
  end

  def get_product_price
    begin
      print 'Price : '
      price = gets.chomp
      validate_price(price)
    rescue BlankValueError, NegativeValueError, NonNumericValueError => e
      process_rescue(e)
      retry
    end
    price.to_f
  end

  def process_rescue(e)
    puts e.error_message, 'Please re-enter', "\n"
  end

  def get_product_from_user
    name = get_product_name
    imported = get_product_imported
    sales_tax_exempted = get_sales_tax_exempted
    price = get_product_price
    Product.new(name, imported, sales_tax_exempted, price, false)
  end

  def get_user_option
    begin
      print 'Do you want to add more items to your list(y/n) : '
      option = gets.chomp
      validate_user_option(option)
    rescue BlankValueError, YNValueError => e
      process_rescue(e)
      retry
    end
    option.downcase
  end

  def add_more_items?
    add_more_items.downcase == 'y'
  end

  def load_product_bucket
    product_bucket = []
    loop do
      product_bucket.push(get_product_from_user)
      puts
      break if get_user_option == 'n'
      clear_screen
    end
    product_bucket
  end

  def display_transaction_summary(product_bucket)
    display_size = {name_width: 30, price_width: 20, sales_tax_width: 20, import_duty_width: 20, total_price_width:20}
    print_line(display_size)
    display_header(display_size)
    print_line(display_size)
    display_bucket(display_size, product_bucket)
  end

  def display_header(display_size)
    print 'Product Name'.ljust(display_size[:name_width], ' ')
    print 'Product Price'.ljust(display_size[:price_width], ' ')
    print 'Sales Tax'.ljust(display_size[:sales_tax_width], ' ')
    print 'Import Duty'.ljust(display_size[:import_duty_width], ' ')
    print 'Total Price'.ljust(display_size[:total_price_width], ' ')
    puts
  end

  def display_bucket(display_size, product_bucket)
    total_price = 0.0
    product_bucket.each do |product|
      total_price += product.total_price
      print product.name.ljust(display_size[:name_width], ' ')
      print product.price.to_s.ljust(display_size[:price_width], ' ')
      print product.sales_tax.to_s.ljust(display_size[:sales_tax_width], ' ')
      print product.import_duty.to_s.ljust(display_size[:import_duty_width], ' ')
      print product.total_price.to_s.ljust(display_size[:total_price_width], ' ')
      puts
    end
    print_line(display_size)
    print 'GRAND TOTAL'.ljust(display_size.values.first(4).inject(:+),' ')
    print total_price.to_s.ljust(display_size[:total_price_width])
    puts
    print_line(display_size)
  end

  def print_line(display_size)
    puts '-'*display_size.values.inject(:+)
  end

  def clear_screen
    print %x{clear}
  end

end
