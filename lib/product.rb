require_relative 'custom_classes.rb'

class Product

  include CustomClasses::CustomValidations
  SALES_TAX_RATE = 10.0
  IMPORT_DUTY_RATE = 5.0

  attr_accessor :name, :price, :imported, :sales_tax_exempted, :validation_flag
  attr_reader :sales_tax, :import_duty, :total_price

  def initialize(name, imported, sales_tax_exempted, price, validation_flag)
    @name               = process_name(name)
    @imported           = process_imported(imported)
    @sales_tax_exempted = process_sales_tax_exempted(sales_tax_exempted)
    @price              = process_price(price)
    @validation_flag    = validation_flag
    @sales_tax          = calculate_sales_tax(price, sales_tax_exempted)
    @import_duty        = calculate_import_duty(price, imported)
    @total_price        = price + sales_tax + import_duty
  end

  def process_name(name)
    validate_product_name(name) unless validation_flag
    name
  end

  def process_imported(imported)
    validate_imported(imported) unless validation_flag
    imported
  end

  def process_sales_tax_exempted(sales_tax_exempted)
    validate_sales_tax_exempted(sales_tax_exempted) unless validation_flag
    sales_tax_exempted
  end

  def process_price(price)
    validate_price(price) unless validation_flag
    price
  end

  def calculate_sales_tax(price, sales_tax_exempted)
    (sales_tax_exempted.downcase == 'yes') ? 0.0 : price * (SALES_TAX_RATE/100)
  end

  def calculate_import_duty(price, imported)
    (imported.downcase == 'no') ? 0.0 : price * (IMPORT_DUTY_RATE/100)
  end

end